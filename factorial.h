#ifndef FACTORIAL_H
#define FACTORIAL_H


class Factorial
{
protected:
    int letter_;
};



class FactorialRecursive : protected Factorial
{
public:
    FactorialRecursive();
    void setLetter(int);
    int getLetter();
    int factoriall(int);
    void display();
    void run();

};

class FactorialFor : protected Factorial
{
private:
    int factorial_;
public:
    FactorialFor();
    void setN(int);
    int getN();
    void setFactorial(int);
    void calculateDisplay();
    void run();

};



#endif // FACTORIAL_H
