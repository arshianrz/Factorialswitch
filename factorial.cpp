#include "factorial.h"
#include <iostream>

using namespace std;

FactorialFor::FactorialFor()
{
    setFactorial(1);
    run();
}

void FactorialFor::setN(int N)
{
    letter_=N;
}

int FactorialFor::getN()
{
    return letter_;
}

void FactorialFor::setFactorial(int FACTORIAL)
{
    factorial_=FACTORIAL;
}



void FactorialFor::calculateDisplay()
{
    for(int i=letter_ ; i>=1 ; i--)
    {
        factorial_*=i;
    }
    if(letter_==0)
    {
        cout<<"Factorial of "<<letter_<<" is : 1 .\n";
    }
    else
    {
        cout<<"Factorial of "<<letter_<<" is : "<<factorial_<<" .\n";
    }
}

void FactorialFor::run()
{
    int x;
    cout<<"Enter a Number. \n";
    cin>>x;
    setN(x);
    getN();
    calculateDisplay();
}
//**************************************************************************************************************
FactorialRecursive::FactorialRecursive()
{
    run();
}

void FactorialRecursive::setLetter(int N)
{
    letter_=N;
}

int FactorialRecursive::getLetter()
{
    return letter_;
}

int FactorialRecursive::factoriall(int f)
{
    if (f==0 || f==1)
    {
        return 1;
    }
    else
        return f*factoriall(f-1);

}
void FactorialRecursive::display()
{
    cout<<"The factorial of "<<letter_<<" is : "<<factoriall(letter_)<<endl;
}

void FactorialRecursive::run()
{
    int x;
    cout<<"Enter a number. \n";
    cin>>x;

    setLetter(x);
    getLetter();
    factoriall(x);
    display();
}
