#include <iostream>
#include <iomanip>
#include "factorial.h"
using namespace std;


void selectWay();
int main()
{
    selectWay();
    return 0;
}
void selectWay()
{
    int x;
    cout<<"In which way you want to calculate factorial \n";
    cout<<"To use recursive way : "<<setw(10)<<"Press 1"<<endl;
    cout<<"To use for way : "<<setw(16)<<"Press 2"<<endl;
    cin>>x;

    switch (x)
    {
    case 1:
        FactorialRecursive();
        break;

    case 2:
        FactorialFor();
        break;

    default:
        break;
    }

}
